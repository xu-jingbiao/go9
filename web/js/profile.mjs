// profile.js
// firstName,lastName,year
// export var firstName = 'Michael';
// export var lastName = 'Jackson';
// export var year = 1958;

var firstName = 'Michael';
var lastName = 'Jackson';
var year = 1958;

// 封装成一个{}, 统一暴露
// export var MyApp = {
//     firstName,
//     lastName,
//     year
// }

// 默认暴露: export default
// export var __buildin__ = {}
export default {
    firstName,
    lastName,
    year
}

// profile.js
// {firstName: firstName, lastName: lastName, year:year}
// export --> {}
// export {firstName, lastName, year};

// if (year > 1900) {
//     console.log(year)
// }

// for (let i= 0;i<100;i++) {
//     console.log(i)
// }