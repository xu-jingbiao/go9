


// json(Restful API Call) ==> {"name": '小明', "age": 23} ==> js Object ===> 处理  ==> 显示(UI)
var person = {name: '小明', age: 23} 
person.greetfn = function() {
    // that = this
    // this
    return () => {
        // this 继承自上层的this
        console.log(`hello, my name is ${this.name}`)
    }
}

person.greetfn()()