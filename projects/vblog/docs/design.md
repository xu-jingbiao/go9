# 项目设计文档

核心需求: 管理员后台发布markdown的博客, 游客在前台浏览博客内容

## 需求

管理员:

+ 对文章的 增删改查, 
+ 文章的发布: 文章在未发布之前是草稿状态, 可以发布文章, 发布过后游客可以在前台看到

游客:

+ 需要浏览文章, 查看文章

## 功能

+ 登录功能: 未登录的人员称为游客, 登录了的人员是管理员, 使用basic auth
+ 文章管理: 文章crud
+ 标签管理: 文章标签的curd


## 原型

[项目原型](./vblog.drawio)


## 流程设计

+ 登录
+ 创建文章, 修改, 最后发布文章
+ 给文章添加标签: Language: Go,   文章 1:N 标签
+ 退出登录


+ 访问浏览文章列表, 可以通过标签搜索文章
+ 浏览具体的文章

## 接口设计

接口按照Restful风格来进行设计

接口的服务端地址: http://localhost:8050, 服务前缀: /vblog/api/v1

登录功能:

+ Login(用户登录): POST /vblog/api/v1/login, 用户登录完成后返回用户信息
    + 请求参数: BasicAuth:    Auth头: username:password(base64编码的字符串)
    + 返回资源: 用户信息
+ Logout(退出登录): POST /vblog/api/v1/logout, 用户退出, 删除用户登录session

文章管理:

+ 管理员(需要认证): 
    + 文章创建: POST /vblog/api/v1/blogs, 
    + 文章修改: PUT /vblog/api/v1/blogs/blog_id
    + 文章删除: DELETE /vblog/api/v1/blogs/blog_id
    + 文章添加标签: POST /vblog/api/v1/blogs/blog_id/tags
    + 文章去除标签: DELETE /vblog/api/v1/blogs/blog_id/tags/tag_id

+ 文章列表查询(文章搜索): GET /vblog/api/v1/blogs
    + 管理员: 可以查看到未发布的文章
    + 游客: 只能查看到已发布文章
+ 查询文章详情: GET /vblog/api/v1/blogs/blog_id
    
标签管理:

+ 标签列表: GET /vblog/api/v1/tags 获取当前系统有哪些标签

## 数据库设计

用户: 不做用户表设计, 用户的信息通过配置文件提供(username:password)

文章: 表名称: blogs
+ id：表示文章主键, 唯一性
+ title: 文章标题, 唯一键, 不允许重复
+ author: 文章的作者
+ created_at: 创建时间
+ updated_at：更新时间
+ pulished_at: 发布时间
+ summary：概要描述
+ content: 文章内容
+ status：文章当前状态,枚举  草稿/已发布

blog01: tag1(Language=GO), tag2(Type=微服务), tag1(Language=GO): blog01, blog02, blog03

标签: 表名称: tags, 标签和文章关系: N: N
+ id: 标签的Id, 全局唯一, 直接通过 Hash(文章+标签key+标签Value)
+ t_key: 标签key, Language 
+ t_value: 标签的值, Go
+ blog_id: 标签管理关联的博客Id
+ created_at: 创建时间, 用于排序



