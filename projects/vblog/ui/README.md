# ui

Vblog 项目前端

## 配置代理模式开发

1. vite 开启代理模式
```js
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), splitVendorChunkPlugin()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    outDir: "../api/ui/dist",
  },
  server: {
    proxy: {
      "/vblog/api/": "http://localhost:8050/",
    },
  },
});
```

2. http客户端 配置(axios)
```js
// 初始化一个http客户端实例
// create an axios instance
const client = axios.create({
  // 由于前端配置代理, 所以不用使用baseURL
  // url = base url + request url
  // 如果使用代理模式: vite.config.js添加配置:
  // server: {
  //   proxy: {
  //     "/vblog/api": {
  //       target: "http://localhost:8050",
  //     },
  //   },
  // },
  // 使用代理模式是, 需要把请求指向到vite服务器
  // 没有配baseURL, 默认会把请求转交给Vite
  baseURL: "",
  // 执行Set Cookie逻辑, 后端CROS Access-Control-Allow-Origin 不能为*
  withCredentials: true,
  // HTTP请求超时时间
  timeout: 5000,
});
```


## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
