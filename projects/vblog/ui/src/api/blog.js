import client from "./client";

// 博客列表
export function LIST_BLOG(params) {
  // 使用封装好的http client进行网络请求
  return client({
    url: "/vblog/api/v1/blogs/",
    method: "get",
    // params 指的是 URL参数
    params: params,
  });
}

// 博客详情
export function DESCRIBE_BLOG(id) {
  // 使用封装好的http client进行网络请求
  return client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: "get",
  });
}

// 创建博客
export function CREATE_BLOG(data) {
  // 使用封装好的http client进行网络请求
  return client({
    url: "/vblog/api/v1/blogs/",
    method: "post",
    // Body 里面的需要传递的JSON对象, JSON <==> Object
    // 文章标题, 唯一键, 不允许重复
    // Title string `json:"title" validate:"required"`
    // // 文章的作者
    // Author string `json:"author" validate:"required"`
    // // 概要描述
    // Summary string `json:"summary" validate:"required"`
    // // 文章内容
    // Content string `json:"content" validate:"required"`
    // // 文章当前状态,枚举  草稿/已发布
    // Status Status `json:"status"`
    // {title: "", author: "", summary: "", content: ""}
    data: data,
  });
}

// 更新博客
export function UPDATE_BLOG(id, data) {
  // 使用封装好的http client进行网络请求
  return client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: "put",
    data: data,
  });
}

// 修改博客(PATCH)
export function PATCH_BLOG(id, data) {
  // 使用封装好的http client进行网络请求
  return client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: "patch",
    data: data,
  });
}

// 删除文章
export function DELETE_BLOG(id) {
  // 使用封装好的http client进行网络请求
  return client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: "delete",
  });
}
