// 计算当前用户是否处于登录中
import { IsLogin } from "@/composables/user";

export async function beforeEachHandler(to, from, next) {
  // 无需处理from
  //   console.log(from);

  // 我们要跳转到那个页面
  //   1. 被保护的页面(需要登录才能访问) -- /backend/xxxxx
  //   2. 公开的页面(不需要登录就能访问)
  // console.log(to);
  // 需要保护的页面
  if (to.fullPath.indexOf("/backend") === 0) {
    // 1. 判断是否登录
    if (IsLogin()) {
      // 已经登录
      next();
    } else {
      // 未登录, 这里是vue-router库的构造, 不用使用useRouter
      // 如果想用使用router进行路由跳转, 使用next参数 == router
      let location = {
        path: "/login",
        // 需要携带之前的页面路径或者名称, 这里我使用名称()
        // 把之前的页面path, url query stirng:  /login?redict=xxx&<query...>
        query: {
          // 目标路由要么的名称, 注意: route一定要定义名称
          redirect: to.name,
          // 目标页面的参数
          ...to.query,
        },
      };
      // console.log(location);
      next(location);
    }
  } else {
    // 不需要保护的页面
    // flow
    next();
  }
}

export function afterEachHandler() {
  // console.log(to, from);
}
