import Cookies from "js-cookie";

// 判断用户是否登录
export function IsLogin() {
  // 通过cookie username属性来进行判断?
  // js 如何操作cookie
  const username = Cookies.get("username");
  return username !== undefined && username !== "";
}
