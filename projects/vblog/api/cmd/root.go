package cmd

import (
	"fmt"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/cmd/initial"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/cmd/start"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/version"
	"github.com/spf13/cobra"
)

// RootCmd, 需要做什么
// 1. 打印帮助信息
// 2. -v 打印当前服务端可执行文件的版本信息

var (
	// 是否打印版本信息
	showVersion bool
)

var RootCmd = &cobra.Command{
	// 程序名称
	Use:     "vblog-api [start|init]",
	Short:   "Vblog Web Service",
	Long:    "前后端分离的博客系统, API Server",
	Example: "vblog-api -v",
	// 命令执行逻辑
	// The *Run functions are executed in the following order:
	//   * PersistentPreRun()
	//   * PreRun()
	//   * Run()
	//   * PostRun()
	//   * PersistentPostRun()
	Run: func(cmd *cobra.Command, args []string) {
		if showVersion {
			fmt.Println(version.Short())
			return
		}

		cmd.Help()
	},
}

// 报导入的时候, 定义CLI参数获取声明: -v
func init() {
	// 无论执行那个子命令 都会显示该参数 varP: -v, var: --verson
	// vblog-api -v (v:true)
	// vblog-api (v:false)
	RootCmd.Flags().BoolVarP(&showVersion, "version", "v", false, "show vblog version")

	// 添加子cmd
	RootCmd.AddCommand(start.Cmd, initial.Cmd)
}
