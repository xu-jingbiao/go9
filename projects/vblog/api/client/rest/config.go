package rest

// 默认值和 server端的默认值 配对
func NewDefaultConfig() *Config {
	return &Config{
		URL:      "http://127.0.0.1:8050/vblog/api/v1",
		Username: "admin",
		Password: "123456",
	}
}

// 访问Restful 接口
type Config struct {
	URL      string
	Username string
	Password string
}
