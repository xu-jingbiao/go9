package rpc_test

import (
	"context"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/client/rpc"
)

var (
	ctx = context.Background()
)

func TestCreateComment(t *testing.T) {
	cs, err := rpc.NewClientSet("localhost:18050")
	if err != nil {
		t.Fatal(err)
	}
	ins, err := cs.Comment().CreateComment(ctx, &comment.CreateCommentRequest{
		Content: "测试",
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
