package apps

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/logger"
	"google.golang.org/grpc"
)

// 托管所有的impl的实例类
// 作为一个Ioc容器 (实例的托儿所)
var (
	grpcAppStore = map[string]GrpcIocObject{}
)

type GrpcIocObject interface {
	IocObject
	RegistryHandler(*grpc.Server)
}

// 1. Registry: 实例的注册
// 不能具体到接口， 具体到接口，导致每个接口都需要写一个注册函数
func RegistryGrpc(obj GrpcIocObject) {
	// 如果该对象已经存在了，还允许注册吗？

	// 判断对象是否已经注册
	if _, ok := httpAppStore[obj.Name()]; ok {
		panic(obj.Name() + "has registied")
	}

	// 存入store
	grpcAppStore[obj.Name()] = obj

	// 2. 补充日志
	logger.L().Debug().Msgf("grpc object %s registried", obj.Name())
}

// 2. Get: 通过名称获取实例类
func GetGrpcApp(objName string) any {
	// 如果获取不到对象是不是应该报错?
	if v, ok := grpcAppStore[objName]; ok {
		return v
	}

	panic("grpc object" + objName + "not found")
}
