package tag

import (
	"context"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/validator"
)

const (
	AppName = "tags"
)

// 接口的定义很关键, 他站在使用的角度来定义
type Service interface {
	// 添加Tag
	CreateTag(context.Context, *CreateTagRequest) (*Tag, error)
	// 查询Tag
	QueryTag(context.Context, *QueryTagRequest) (*TagSet, error)
	// 移除Tag
	RemoveTag(context.Context, *RemoveTagRequest) (*TagSet, error)
}

func NewCreateTagRequest(key, value string, blogId int) *CreateTagRequest {
	return &CreateTagRequest{
		Key:    key,
		Value:  value,
		BlogId: blogId,
	}
}

func NewDefaultCreateTagRequest() *CreateTagRequest {
	return &CreateTagRequest{}
}

type CreateTagRequest struct {
	Key    string `gorm:"column:t_key" json:"key" validate:"required"`
	Value  string `gorm:"column:t_value" json:"value" validate:"required"`
	BlogId int    `gorm:"column:blog_id" json:"blog_id" validate:"required"`
}

func (req *CreateTagRequest) Validate() error {
	return validator.V().Struct(req)
}

func NewQueryTagRequest() *QueryTagRequest {
	return &QueryTagRequest{
		BlogIds: []int{},
	}
}

type QueryTagRequest struct {
	// 查询某一篇文章的Tag, 如果你想要批量查询
	// 创建API性能优化的方式(Batch 做批量合并操作)
	BlogIds []int
}

func (req *QueryTagRequest) Add(ids ...int) {
	req.BlogIds = append(req.BlogIds, ids...)
}

type RemoveTagRequest struct {
	// 一次移除多个Tag
	TagsIds []int
}
