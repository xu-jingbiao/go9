package impl

import (
	"context"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/tag"
)

func (i *impl) CreateTag(ctx context.Context, req *tag.CreateTagRequest) (
	*tag.Tag, error) {
	// 自己代码: 当前包上的短点
	ins, err := tag.NewTag(req)
	if err != nil {
		return nil, err
	}

	// 自己代码: 其他包
	// 查询属于bookA 的Tag,
	// i.blog.DescribeBlog() 确认blog 是存在的
	_, err = i.blog.DescribeBlog(ctx, blog.NewDescribeBlogRequest(req.BlogId))
	if err != nil {
		return nil, err
	}

	// 其他第三方包:d 打断点 或者标准库
	// 对象入库
	// INSERT INTO `tags` (`key`,`value`,`blog_id`,`create_at`) VALUES ('分类','GO语言',1,1670121153)
	// Save UpSert 如果主键冲突  Duplicate entry '2fKKsA==' for key 'tags.PRIMARY', 会执行UPdate
	if err := i.db.WithContext(ctx).Create(ins).Error; err != nil {
		return nil, err
	}

	return ins, nil
}

func (i *impl) QueryTag(ctx context.Context, req *tag.QueryTagRequest) (
	*tag.TagSet, error) {
	query := i.db.Table("tags").WithContext(ctx)

	if len(req.BlogIds) > 0 {
		// blog_id IN (0, 1, 2)
		query = query.Where("blog_id IN (?)", req.BlogIds)
	}

	set := tag.NewTagSet()
	err := query.
		Order("created_at DESC").
		Find(&set.Items).
		Error
	if err != nil {
		return nil, err
	}

	set.Total = int64(len(set.Items))

	return set, nil
}

func (i *impl) RemoveTag(ctx context.Context, req *tag.RemoveTagRequest) (
	*tag.TagSet, error) {
	return nil, nil
}
