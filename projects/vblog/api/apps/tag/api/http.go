package api

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/tag"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/protocol/middleware"
	"github.com/gin-gonic/gin"
)

// 定义一个handler对象来 实现 HTTP接口
// 需要依赖业务逻辑, 业务实例类
type handler struct {
	svc tag.Service
}

func (h *handler) Init() error {
	// ioc 依赖, 需要注入 user servcve的实例类
	// 通过ioc获取依赖
	h.svc = apps.GetInternalApp(tag.AppName).(tag.Service)
	return nil
}

func (h *handler) Name() string {
	return tag.AppName
}

// 这个模块的API的前缀: /vblog/api/v1/tags
// blog handler 需要暴露的接口 注册给 gin root router
func (h *handler) RegistryHandler(r gin.IRouter) {
	// middleware.AuthFunc 会作用于后面的所有接口
	r.Use(middleware.AuthFunc)

	r.POST("/add", h.CreateTag)
	r.POST("/remove", h.RemoveTag)
}

func init() {
	apps.RegistryHttp(&handler{})
}
