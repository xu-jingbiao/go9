package apps

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/logger"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

// 初始化所有的托管实例
// 这个初始化应该在什么时候进行? 放置main
func InitApps() error {
	for _, v := range internalAppStore {
		if err := v.Init(); err != nil {
			return err
		}
		// 补充日志
		logger.L().Debug().Msgf("object %s initial", v.Name())
	}
	return nil
}

func InitHttpApps(r gin.IRouter) error {
	for _, v := range httpAppStore {
		// 实例初始化
		if err := v.Init(); err != nil {
			return err
		}

		// 针对Http App 的实例类, 需要注册给 root router
		// group: v1 := server.Group("/vblog/api/v1")
		// 我们有很多个http 接口模块, 每个接口模块 是不知道对方存在的, 可能会出现逻辑冲突
		// 可以尝试添加 模块名称作为 逻辑的前缀
		// /vblog/api/v1/<app_name>, 类似于添加了 名称空间(namespace)
		v.RegistryHandler(r.Group(v.Name()))

		// 补充日志
		logger.L().Debug().Msgf("http object %s initial", v.Name())
	}

	return nil
}

func InitGrpcApps(server *grpc.Server) error {
	for _, v := range grpcAppStore {
		// 实例初始化
		if err := v.Init(); err != nil {
			return err
		}

		v.RegistryHandler(server)

		// 补充日志
		logger.L().Debug().Msgf("grpc object %s initial", v.Name())
	}

	return nil
}

// 不能，进行对象初始化的时候，有前提条件:
// 1. 所有对象已经注册
// 2. config需要提前初始化
// 需要放置main.go
// func init() {
// 	InitApps()
// }
