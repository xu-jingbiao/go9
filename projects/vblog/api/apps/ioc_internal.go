package apps

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/logger"
)

// 托管所有的impl的实例类
// 作为一个Ioc容器 (实例的托儿所)
var (
	internalAppStore = map[string]IocObject{}
)

type IocObject interface {
	// 需要获取对象的名称
	Name() string
	// 体统对象初始化能力
	Init() error
}

// 1. Registry: 实例的注册
// 不能具体到接口， 具体到接口，导致每个接口都需要写一个注册函数
func Registry(obj IocObject) {
	// 如果该对象已经存在了，还允许注册吗？

	// 判断对象是否已经注册
	if _, ok := internalAppStore[obj.Name()]; ok {
		panic(obj.Name() + "has registied")
	}

	// 存入store
	internalAppStore[obj.Name()] = obj

	// 2. 补充日志
	logger.L().Debug().Msgf("object %s registried", obj.Name())
}

// 2. Get: 通过名称获取实例类
func GetInternalApp(objName string) any {
	// 如果获取不到对象是不是应该报错?
	if v, ok := internalAppStore[objName]; ok {
		return v
	}

	panic(objName + "not found")
}
