package api

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/protocol/middleware"
	"github.com/gin-gonic/gin"
)

// 定义一个handler对象来 实现 HTTP接口
// 需要依赖业务逻辑, 业务实例类
type handler struct {
	svc blog.Service
}

func (h *handler) Init() error {
	// ioc 依赖, 需要注入 user servcve的实例类
	// 通过ioc获取依赖
	h.svc = apps.GetInternalApp(blog.AppName).(blog.Service)
	return nil
}

func (h *handler) Name() string {
	return blog.AppName
}

// 这个模块的API的前缀: /vblog/api/v1/blogs
// blog handler 需要暴露的接口 注册给 gin root router
func (h *handler) RegistryHandler(r gin.IRouter) {
	// 不需要认证的接口: 访问可用访问
	// GET /vblog/api/v1/blogs/
	r.GET("/", h.QueryBlog)
	// GET /vblog/api/v1/blogs/blog_id
	r.GET("/:id", h.DescribeBlog)

	// middleware.AuthFunc 会作用于后面的所有接口
	r.Use(middleware.AuthFunc)
	// 需要认证的接口: 需要登录, 管理员使用的
	// POST /vblog/api/v1/blogs
	r.POST("/", h.CreateBlog)
	// PUT/PATCH /vblog/api/v1/blogs/blog_id
	r.PUT("/:id", h.PutBlog)
	r.PATCH("/:id", h.PatchBlog)
	// DELETE /vblog/api/v1/blogs/blog_id
	r.DELETE("/:id", h.DeleteBlog)
}

func init() {
	apps.RegistryHttp(&handler{})
}
