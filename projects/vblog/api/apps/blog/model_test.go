package blog_test

import (
	"context"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
)

var (
	ctx = context.Background()
)

func TestXxx(t *testing.T) {
	req := blog.CreateBlogRequest{
		Status: blog.DRAFT,
	}
	t.Log(req)

	var svc blog.Service
	// svc.QueryBlog(ctx, pageSize, pageNumber, ...)
	svc.QueryBlog(ctx, &blog.QueryBlogRequest{})
}

func TestString(t *testing.T) {
	b := &blog.Blog{
		Id: 1,
		CreateBlogRequest: &blog.CreateBlogRequest{
			Title: "xxxx",
		},
	}
	// Log 也是调用print进行打印的
	t.Log(b)
}
