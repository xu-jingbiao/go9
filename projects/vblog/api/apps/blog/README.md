# 文章管理

## 接口

+ 管理员(需要认证): 
    + 文章创建: POST /vblog/api/v1/blogs, 
    + 文章修改: PUT /vblog/api/v1/blogs/blog_id
    + 文章删除: DELETE /vblog/api/v1/blogs/blog_id
    + 文章添加标签: POST /vblog/api/v1/blogs/blog_id/tags
    + 文章去除标签: DELETE /vblog/api/v1/blogs/blog_id/tags/tag_id

+ 文章列表查询(文章搜索): GET /vblog/api/v1/blogs
    + 管理员: 可以查看到未发布的文章
    + 游客: 只能查看到已发布文章
+ 查询文章详情: GET /vblog/api/v1/blogs/blog_id

## 存储表结构

文章: 表名称: blogs
+ id：表示文章主键, 唯一性
+ title: 文章标题, 唯一键, 不允许重复
+ author: 文章的作者
+ created_at: 创建时间
+ updated_at：更新时间
+ pulished_at: 发布时间
+ summary：概要描述
+ content: 文章内容
+ status：文章当前状态,枚举  草稿/已发布

标签: 表名称: tags, 标签和文章关系: N: N
+ id: 标签的Id, 全局唯一, 直接通过 Hash(文章+标签key+标签Value)
+ t_key: 标签key, Language 
+ t_value: 标签的值, Go
+ blog_id: 标签管理关联的博客Id
+ created_at: 创建时间, 用于排序

定义了接口和数据结构

## 实现了业务对象

impl 业务对象, 业务控制器

## 实现Restful 接口

