package apps_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
)

var (
	ctx = context.Background()
)

const (
	AppName = "blog_service_impl"
)

type AppObject struct {
}

func (o *AppObject) Name() string {
	return AppName
}

func (o *AppObject) Init() error {
	return nil
}

func (o *AppObject) CreateBlog(context.Context, *blog.CreateBlogRequest) (
	*blog.Blog, error) {
	return nil, fmt.Errorf("not implement")
}

func TestRegistry(t *testing.T) {
	// 对象的注册
	apps.Registry(&AppObject{})

	//如果获取对象
	obj := apps.GetInternalApp(AppName)
	// 断言对象类型
	blogSvc := obj.(blog.Service)
	// 使用对象
	b, err := blogSvc.CreateBlog(ctx, nil)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(b)
}
