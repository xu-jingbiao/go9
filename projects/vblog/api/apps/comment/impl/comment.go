package impl

import (
	"context"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
)

// 添加评论
func (i *impl) CreateComment(ctx context.Context, in *comment.CreateCommentRequest) (
	*comment.Comment, error) {

	// 1. 请求参数的校验, 如何进行参数校验
	if err := in.Validate(); err != nil {
		return nil, err
	}

	ins := comment.NewComment(in)

	// 3. 把对象保存到数据库, Mysql, 需要操作数据库
	// DB 不是指的一个连接，指的一个连接池
	// 可以通过 sql.DB 这个连接池来与数据库进行交互
	// INSERT INTO `blogs` (`created_at`,`updated_at`,`pulished_at`,`title`,`author`,`summary`,`content`,`status`) VALUES (1669445556,1669445556,0,'Go Course1','oldfish','Go Javascript','baba',0)
	if err := i.db.WithContext(ctx).Save(ins).Error; err != nil {
		return nil, err
	}
	// ORM
	return ins, nil
}
