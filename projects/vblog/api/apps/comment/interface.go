package comment

const (
	AppName = "comments"
)

type Service interface {
	RPCServer
	// 编写非RPC的进程内调用接口
	// DeleteComment(context.Context, *DeleteCommentRequest) (*Comment, error)
}
