package api

import (
	"net/http"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
	"github.com/gin-gonic/gin"
)

// 如何测试你这个功能是否正常: Postman
func (h *handler) CreateComment(c *gin.Context) {
	req := comment.NewCreateCommentRequest()
	// 如何获取 用户通过http body传递过来的请求数据
	// 这样就读取到 http body的数据 io.ReadAll(c.Request.Body)
	// 每个HTTP 框架 Objct -- Body

	if err := c.BindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	ins, err := h.svc.CreateComment(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	// 进行返回的时候, 转换成立需要兼容的数据结构

	c.JSON(http.StatusOK, ins)
}
