package user

import (
	"context"
	"net/http"
)

const (
	AppName = "user"
)

// 接口的定义很关键, 他站在使用的角度来定义
type Service interface {
	// 用户登录
	Login(context.Context, *LoginRequest) (*Session, error)
	// 用户登出
	Logout(context.Context, *LogoutRequest) error
	// 检查用户是否登录
	CheckIsLogin(context.Context, *CheckIsLoginRequest) error
}

func NewCheckIsLogin(username, session string) *CheckIsLoginRequest {
	return &CheckIsLoginRequest{Username: username, SessionId: session}
}

type CheckIsLoginRequest struct {
	Username  string
	SessionId string
}

// 构造函数？
func NewLoginRequest() *LoginRequest {
	return &LoginRequest{}
}

func NewLoginRequestFromBasicAuth(r *http.Request) *LoginRequest {
	user, pass, _ := r.BasicAuth()
	return &LoginRequest{
		Name: user,
		Pass: pass,
	}
}

type LoginRequest struct {
	Name string
	Pass string
}

// 构造函数？
func NewLogoutRequest() *LogoutRequest {
	return &LogoutRequest{}
}

type LogoutRequest struct {
	Username string `json:"username"`
}
