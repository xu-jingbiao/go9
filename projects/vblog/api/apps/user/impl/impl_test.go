package impl_test

import (
	"context"
	"os"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/user"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/test/tools"
)

var (
	svc user.Service
	ctx = context.Background()
)

func TestLogin(t *testing.T) {
	// 登录
	req := user.NewLoginRequest()
	req.Name = os.Getenv("AUTH_USERNAME")
	req.Pass = os.Getenv("AUTH_PASSWORD")
	sess, err := svc.Login(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(sess)

	// 验证是否登录
	username := os.Getenv("AUTH_USERNAME")
	checkReq := user.NewCheckIsLogin(username, sess.Id)
	err = svc.CheckIsLogin(ctx, checkReq)
	if err != nil {
		t.Fatal(err)
	}
}

func TestLogout(t *testing.T) {
	req := user.NewLogoutRequest()
	err := svc.Logout(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
}

// 传入实例类
func init() {
	// 加载测试用例的配置对象
	tools.DevelopmentSet()

	// 从ioc中获取被测试的实例类，来进行测试
	svc = apps.GetInternalApp(user.AppName).(user.Service)
}
