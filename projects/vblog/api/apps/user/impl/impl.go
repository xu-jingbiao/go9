package impl

import (
	"sync"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/user"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/conf"
)

// 负责实现service
// 用户密码和密码放到配置上的, 依赖配置对象
type impl struct {
	Auth *conf.Auth

	// 使用一个map保存登录用户的session
	// 用户有没有登录是维护在服务器的 sessions map里面的
	// {expired, session}  (可以使用Redis 来进行改进优化)
	sessions map[string]string
	lock     sync.Mutex
}

// 对实例进行初始化，保护依赖的注入
func (i *impl) Init() error {
	// 通过ioc完成依赖的手动注入
	// &impl{
	// 	Auth:     conf.C().Auth,
	// 	sessions: map[string]string{},
	// }

	i.Auth = conf.C().Auth
	i.sessions = map[string]string{}
	return nil
}

// 其他地方还需要依赖这个名字
func (i *impl) Name() string {
	return user.AppName
}

func init() {
	apps.Registry(&impl{})
}
