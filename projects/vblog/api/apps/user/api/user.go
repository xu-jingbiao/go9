package api

import (
	"net/http"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/user"
	"github.com/gin-gonic/gin"
)

// Gin Context 不是 context.Context
// Gin HTTP 标准库的 Request对象和 Response对象封装在一个统一的结构体上
// 叫 Gin.Context
func (h *handler) Login(c *gin.Context) {
	// 这里的Ctx怎么传递？
	// 需要HTTP 的context,  HTTP Request的Context
	req := user.NewLoginRequestFromBasicAuth(c.Request)

	// 读取 Http Body 里面得数据
	// c.BindJSON()
	// 直接把Login的业务逻辑写在这里
	// 测试用例 必须基于HTTP写, 编写会比较麻烦
	// 你这个业务功能别其他模块使用(内部/RPC), 都比较不利用复用
	sess, err := h.svc.Login(c.Request.Context(), req)
	if err != nil {
		// 返回异常
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": err.Error(),
		})
		return
	}

	// 通过HTTP的response 来设置cookie, 需要配置浏览器才能验证
	// httpOnly: 是否允许前端操作这些Cookie
	c.SetCookie("username", sess.UserName, 60*60, "", "", false, false)
	c.SetCookie("session", sess.Id, 60*60, "", "", false, false)

	// 返回成功
	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": sess,
	})
}

func (h *handler) Logout(c *gin.Context) {
	req := user.NewLogoutRequest()
	// Logout的参数, 需要从HTTP Body中 读取JSON {"username": "xxxxx"}
	// 读取Gin Body参数
	err := c.BindJSON(req)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusBadRequest,
			"message": err.Error(),
		})
	}
	err = h.svc.Logout(c.Request.Context(), req)
	if err != nil {
		// 返回异常
		c.JSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"message": err.Error(),
		})
	}

	// 返回成功
	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "logout ok",
	})
}
