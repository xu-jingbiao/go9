package api

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/user"
	"github.com/gin-gonic/gin"
)

// 定义一个handler对象来 实现 HTTP接口
// 需要依赖业务逻辑, 业务实例类
type handler struct {
	svc user.Service
}

// 依赖svc服务的 实例类
// func NewHandler(svc user.Service) *Handler {
// 	return &Handler{
// 		svc: svc,
// 	}
// }

func (h *handler) Init() error {
	// ioc 依赖, 需要注入 user servcve的实例类
	// 通过ioc获取依赖
	h.svc = apps.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func (h *handler) Name() string {
	return user.AppName
}

// 这个模块的API的前缀: /vblog/api/v1/user
// 把user handler 需要暴露的接口 注册给 gin root router
func (h *handler) RegistryHandler(r gin.IRouter) {
	r.POST("/login", h.Login)
	r.POST("/logout", h.Logout)
}

func init() {
	apps.RegistryHttp(&handler{})
}
