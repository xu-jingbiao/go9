package tools

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/conf"

	// 加载所有的实例类
	_ "gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/all"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
)

// 开发环境 单元测试的Setup
func DevelopmentSet() {
	// 测试测试用例的配置
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	// 初始化测试类的注册
	if err := apps.InitApps(); err != nil {
		panic(err)
	}
}
