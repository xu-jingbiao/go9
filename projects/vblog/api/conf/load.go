package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

// 项目的配置对象, 做成全局的最方便
// 通过LoadConfigFromToml、LoadConfigFromEnv 来加载全局变量
// 通过C() 来访问全局变量
var (
	c *Config
)

func C() *Config {
	if c == nil {
		panic("load config first")
	}
	return c
}

// 给你程序以默认配置
func LoadConfigFromToml(filePath string) error {
	// c 不是全局变量c, 新的变量
	// c := NewDefaultConfig()

	// 用默认值 赋值给全局变量c
	c = NewDefaultConfig()
	_, err := toml.DecodeFile(filePath, c)
	if err != nil {
		return err
	}
	return nil
}

// 使用这个库来进行环境变量映射 github.com/caarlos0/env/v6
func LoadConfigFromEnv() error {
	// 用默认值 赋值给全局变量c
	c = NewDefaultConfig()
	// fmt.Println("AUTH_USERNAME", os.Getenv("AUTH_USERNAME"))
	return env.Parse(c)
}
