# 项目后端

+ main.go: 项目入口文件
+ dist: 不是一个包, 存放项目构建文件的目录, vblog-api
+ conf: 项目配置管理对象, 项目配置加载
    + config.go: 负责定义项目配置对象
    + load.go: 用于加载配置对象, toml, env, ....
+ cmd: 项目的CLI接口,  vblgo service start -f .../config.toml
    + root.go: CLI的入口命令
+ etc: 不是Go 的package, 用于存储项目配置文件的地方
    + config.toml: toml格式项目配置文件
    + unit_test.env: 测试用例 环境变量配置
+ docs: 存放项目文档
+ protocol: 项目协议处理模块, 
  + grpc.go：grpc server, 用于暴露内部Grpc服务(x)
  + http.go: http server, 暴露HTTP接口, 供前端使用(V)
+ apps: 
    + blog: 文章管理模块
    + tag: 标签管理模块
    + user: 登录管理模块
        + interface.go: user模块功能接口定义
        + model.go: user模块数据结构定义
        + impl: user模块的服务实现类
            + impl.go: 实现类的定义
            + impl_test.go: 实现类的单元测试
            + user.go: 实现的业务实现逻辑


## 如何开始

+ 从上往下: 先写逻辑框架
    + http server
    + api, http handler
    + impl
    + 基于Postman测试

+ 从下往上: 先写业务, 后续衔接逻辑
    + interface: 先定义清楚业务
    + impl: 业务实现类
    + impl_test: 测试你的实例类 (TDD)
    + api,http handler
    + http server
    + 基于Postman测试

## 独立开发项目的角色

+ 架构或者框架 制定(架构师或者你们开发组长)
+ 业务编码

## 基于Ioc 的V2版本

+ 业务编码人员 需要频繁去修改 main.go 进行 服务的加载(Gin Server)
+ Book业务以来Tag业务的接口，怎么办? 使用Ioc

Ioc功能: 管理托管 业务实现类, 也能托管http接口实现类
+ 注册/获取
+ 对注册的对象进行统一管理
    + 对象初始化

## 编写业务模块

1. 补全认证逻辑


## 作业



## 编程

程序员的关键技能: debug

+ 直接开发功能(会遇到一些非常奇怪的问题)
+ 接手别人项目(需要关键步骤上进行debug)

技能:
+ 单元测试Debug
   + 编写测试用例
   + 关键部分打上断点(主流程断点，关键位置断点): 代码的执行流程上任务位置都可以打
     + 自己的代码: 单元测试当前包
     + 自己的其他包: 依赖模块
     + 其他依赖(非自己写代码): 第三方库和标准库
   + 开启debug模式(准备好单元测试的环境变量: vscode: settting.json go.testEnvFile)
+ 程序的Debug
   + 配置程序以debug模式启动(vscode: launch.json: configurations)
   + 程序使用的配置,自己通过参数指定(go run main.go -f etc/config.toml)
   + 在需要调试的流程(Restful API), 在需要调试的接口内部打上断点
   + 选择Debug run(vscode: 左侧的运行与调试)
   + 通过工具(Postman/SDK), 触发断点所在流程(调用Restful API)
+ 远程Debug(线上问题排查, 我基本不用)


## vlog 工程化

### CLI

这个不算CLI, 程序没有办法指定配置文件的位置
```go
conf.LoadConfigFromToml("etc/config.toml")
```

```
go run main.go
```

+ 需要初始化(go run main.go init())
+ ...


CLI: 通过命令行可以与程序交互, 通过Flag传参
```
// 编译好的可执行文件: vblog-api
vblog-api start -f etc/config.toml
vblog-api init -f etc/config.toml
```

CLI工具:
+ 标准库: 	flag.Arg() 读取命令行参数  vblog-api start  arg[0]=start
```go
// arg0 := flag.Arg(0)
// flag.Parse()
```
+ 第三方库: [cobra](https://github.com/spf13/cobra): 比较漂亮的CLI  
```sh
$ docker 

Usage:  docker [OPTIONS] COMMAND        

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default
                           "C:\\Users\\yumaojun\\.docker")
  -c, --context string     Name of the context to use to connect to the
                           daemon (overrides DOCKER_HOST env var and
                           default context set with "docker context use")
  -D, --debug              Enable debug mode
  -H, --host list          Daemon socket(s) to connect to
  -l, --log-level string   Set the logging level
                           ("debug"|"info"|"warn"|"error"|"fatal")
                           (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default
                           "C:\\Users\\yumaojun\\.docker\\ca.pem")
      --tlscert string     Path to TLS certificate file (default
                           "C:\\Users\\yumaojun\\.docker\\cert.pem")
      --tlskey string      Path to TLS key file (default
                           "C:\\Users\\yumaojun\\.docker\\key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  builder     Manage builds
  buildx*     Docker Buildx (Docker Inc., v0.7.1)
  compose*    Docker Compose (Docker Inc., v2.2.3)
  config      Manage Docker configs
```


### 版本号注入

```
$ ./dist/vblog-api.exe -v
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_http.go:33 > ***http object blogs registried****
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_http.go:33 > ***http object tags registried****
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_http.go:33 > ***http object user registried****
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_internal.go:34 > ***object blogs registried****
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_internal.go:34 > ***object tags registried****
2022-12-10T11:38:38+08:00 | DEBUG | apps\ioc_internal.go:34 > ***object user registried****
vblog api 1.0.0
```

版本号 应该有哪些信息
+ 版本信息(发版信息): v1.0.0 以Tag作为发版: git tag v1.0.0
+ Commit号: 对应的版本代码: git log
+ 构建的环境: go.19 window/linux arm/amd64: go env
+ 构建日期: 2022-12-10 用于追溯问题: 自动生成

```go
var (
	GIT_TAG    string
	GIT_COMMIT string
	GIT_BRANCH string
	BUILD_TIME string
	GO_VERSION string
)
```

需要在程序构建时，动态注入: 依赖构建参数来进行注入
```
go build  -ldflags "-X <pkg.Var>='<Value>'" "-X <pkg.Var>='<Value>'" ...
```

```
go build  -ldflags "-X 'gitee.com/go-course/go9/tree/master/projects/vblog/api/version.GIT_TAG=v1.0.0' -X 'gitee.com/go-course/go9/tree/master/projects/vblog/api/version.GIT_COMMIT=d80406a5xxxxxx'" -o dist/vblog-api.exe main.go
```

### Makefile

辅助程序开发

https://gitee.com/infraboard/go-course/blob/master/day14/demo-api.md#makefile

常量
```
PROJECT_NAME=api
MAIN_FILE=main.go
```

变量的值 是执行命令生成
```
# 引用之前的变量$(PROJECT_NAME)
PKG := "gitee.com/infraboard/go-course/day14/demo/$(PROJECT_NAME)"

# 执行shell命令 $(shell go env GOMODCACHE)
MOD_DIR := $(shell go env GOMODCACHE)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# 需要注入的变量
BUILD_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
BUILD_COMMIT := ${shell git rev-parse HEAD}
BUILD_TIME := ${shell date '+%Y-%m-%d %H:%M:%S'}
BUILD_GO_VERSION := $(shell go version | grep -o  'go[0-9].[0-9].*')
VERSION_PATH := "${PKG}/version"
```

定义Make命令 make dep, @是否打印执行的命令
```
dep: ## Get the dependencies
    @go mod tidy
```


### 编译优化

https://gitee.com/infraboard/go-course/blob/master/day14/demo-api.md#%E7%BC%96%E8%AF%91%E4%BC%98%E5%8C%96

## 打包前端(ui)

1. 编译出UI代码(产物放到当前项目vblog-api)

执行了npm run build -->  会在当前目录生成 dist目录, 里面是编译后的静态文件, 
ui项目 vite配置添加如下:
```js
  build: {
    outDir: "../api/ui/dist",
  },
```

2. 打包这些静态文件(ui/dist 目录)

go 静态文件打包: go:embed, 会把ui/dist 目录下的文件打包为一个 embed.FS对象
```go
//go:embed dist/index.html
var indexFs embed.FS

//go:embed dist/assets/*
var assetsFs embed.FS
```

3. 使用gin 提供静态页面
```go
    // 默认打包后的文件是全路径 dist/assets/a(文件路径)  ---> dist/assets/a(URL地址)
    // assets/a   ---> a(需要把这些文件 进行路径转换, io/fs 提供的FS函数, 提供子文件系统)
	assets, _ := fs.Sub(assetsFs, "dist/assets")
	r.StaticFS("/assets", http.FS(assets))
```

4. 添加index页面

vblog-api 是个Gin 服务器, / --> http.FS(assets), 所有的请求都指向静态服务 (和 RESTful 接口路径冲突)
单独添加路由处理


## Grpc 处理

1. 如果为你的protobuf messag 添加自定义标签 (struct tag)

使用到的工具: https://github.com/favadi/protoc-go-inject-tag

```
go install github.com/favadi/protoc-go-inject-tag@latest
```

生成代码过后，额外使用该命令来添加自定义标签
```
protoc-go-inject-tag -input=apps/*/*.pb.go
```

如何补充:
```proto3
// file: test.proto
syntax = "proto3";

package pb;
option go_package = "/pb";

message IP {
  // @gotags: valid:"ip"
  string Address = 1;

  // Or:
  string MAC = 2; // @gotags: validate:"omitempty"
}
```


```sh
# protof编译
$ make gen
# 修改
```