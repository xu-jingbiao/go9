package auth

import (
	"strings"

	"gitee.com/go-course/go9/projects/devcloud/mpaas/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/restful/response"
)

func NewHttpAuther() *httpAuther {
	return &httpAuther{}
}

type httpAuther struct {
}

// FilterFunction definitions must call ProcessFilter on the FilterChain to pass on the control and eventually call the RouteFunction
func (a *httpAuther) AuthFunc(
	req *restful.Request,
	resp *restful.Response,
	next *restful.FilterChain) {
	logger.L().Info().Msgf("%s", req)
	// 请求拦截并处理
	// 检查Token令牌 如果不合法 Authorization: breaer xxxx

	// 从Header中获取token
	authHeader := req.HeaderParameter("Authorization")
	tkl := strings.Split(authHeader, " ")
	if len(tkl) != 2 {
		response.Failed(resp, exception.NewUnauthorized("令牌不合法, 格式： Authorization: breaer xxxx"))
		return
	}

	tk := tkl[1]
	logger.L().Debug().Msgf("get token: %s", tk)

	// 检查Token的合法性, 需要通过RPC进行检查

	// 交给下个处理
	next.ProcessFilter(req, resp)

	// 处理相应
	logger.L().Info().Msgf("%s", resp)
}
