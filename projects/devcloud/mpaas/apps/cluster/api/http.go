package api

import (
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"

	"gitee.com/go-course/go9/projects/devcloud/mpaas/apps/cluster"
)

var (
	h = &handler{}
)

type handler struct {
	service cluster.Service
}

func (h *handler) Config() error {
	return nil
}

func (h *handler) Name() string {
	return cluster.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"集群管理"}
	ws.Route(ws.GET("/").To(h.QueryCluster).
		Doc("查询托管集群列表").
		// 对路由进行了装饰, auth为true则开启, false则关闭认证
		Metadata("auth", true).
		Metadata(restfulspec.KeyOpenAPITags, tags))
}

func init() {
	app.RegistryRESTfulApp(h)
}
