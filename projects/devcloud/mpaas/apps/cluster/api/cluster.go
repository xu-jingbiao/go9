package api

import (
	"fmt"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
)

// 令牌颁发
func (h *handler) QueryCluster(r *restful.Request, w *restful.Response) {
	// 获取当前调用者的身份
	attrTk := r.Attribute(token.ATTRIBUTE_TOKEN_KEY)
	fmt.Println(attrTk)
	if attrTk != nil {
		tk := attrTk.(*token.Token)
		fmt.Println(tk)
	}

	// 获取用户参数, 强制 content type 来识别body里面内容的格式
	w.Write([]byte("test"))

}
