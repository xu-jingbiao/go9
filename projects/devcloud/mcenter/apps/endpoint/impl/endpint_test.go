package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/endpoint"
)

func TestRegistryEndpoint(t *testing.T) {
	req := &endpoint.RegistryRequest{
		Items: []*endpoint.CreateEndpointRequest{},
	}
	req.Items = append(req.Items, &endpoint.CreateEndpointRequest{
		ServiceId: "xxx",
		Method:    "POST",
		Path:      "xxxx",
		Operation: "xxx",
	})
	set, err := impl.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
