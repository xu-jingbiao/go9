package impl

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/endpoint"
)

// 注册服务的功能列表
func (i *impl) RegistryEndpoint(ctx context.Context, in *endpoint.RegistryRequest) (
	*endpoint.EndpointSet, error) {
	set := endpoint.NewEndpiontSet()
	// 功能列表可以
	for m := range in.Items {
		r := in.Items[m]
		ep := endpoint.New(r)
		_, err := i.col.InsertOne(ctx, ep)
		if err != nil {
			return nil, err
		}
		set.Items = append(set.Items, ep)
	}
	return set, nil
}
