package impl

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/service"
	"go.mongodb.org/mongo-driver/bson"
)

// 服务的创建
func (i *impl) CreateService(ctx context.Context, in *service.CreateServiceRequest) (
	*service.Service, error) {
	ins := service.New(in)

	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 查询用户详情
func (i *impl) DescribeService(ctx context.Context, in *service.DescribeServiceRequest) (
	*service.Service, error) {
	// 请求校验

	filer := bson.M{}
	switch in.DescribeBy {
	case service.DESCRIBE_BY_SERVICE_ID:
		filer["_id"] = in.DescribeVaule
	case service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID:
		filer["credentail.client_id"] = in.DescribeVaule
	}

	ins := service.NewDefaultUser()
	err := i.col.FindOne(ctx, filer).Decode(ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}
