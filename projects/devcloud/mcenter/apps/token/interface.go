package token

import (
	context "context"
	"fmt"
)

const (
	AppName = "tokens"
)

type Service interface {
	// 令牌颁发: Restful
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	RPCServer
}

func NewValidateTokenRequest(ak string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: ak,
	}
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{}
}

func (req *IssueTokenRequest) Validate() error {
	switch req.GrantType {
	case GRANT_TYPE_PASSWORD, GRANT_TYPE_LDAP:
		if req.Username == "" || req.Password == "" {
			return fmt.Errorf("用户名或者密码缺失")
		}
	}
	return nil
}
