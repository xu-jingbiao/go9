package password

import (
	"context"
	"fmt"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token/provider"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/user"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/logger"
	"github.com/infraboard/mcube/app"
	"github.com/rs/xid"
)

type issuer struct {
	user user.Service
}

func (i *issuer) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (
	*token.Token, error) {
	// 查询该用户
	req := user.NewDescribeUserRequestByUsername(in.Username)
	u, err := i.user.DescribeUser(ctx, req)
	if err != nil {
		return nil, err
	}

	// 检查密码
	err = u.CheckPassword(in.Password)
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("用户名或者密码错误")
	}

	// 颁发令牌
	tk := token.NewToken()
	tk.AccessToken = xid.New().String()
	tk.RefreshToken = xid.New().String()

	return tk, nil
}

func (i *issuer) Config() error {
	i.user = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func init() {
	provider.Registry(token.GRANT_TYPE_PASSWORD, &issuer{})
}
