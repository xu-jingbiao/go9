package auth

import (
	"strings"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/client/rpc"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/restful/response"
)

func NewHttpAuther(client *rpc.ClientSet) *httpAuther {
	return &httpAuther{
		client: client,
	}
}

type httpAuther struct {
	client *rpc.ClientSet
}

// FilterFunction definitions must call ProcessFilter on the FilterChain to pass on the control and eventually call the RouteFunction
func (a *httpAuther) AuthFunc(
	req *restful.Request,
	resp *restful.Response,
	next *restful.FilterChain) {
	logger.L().Info().Msgf("%s", req)
	// 请求拦截并处理
	// 检查Token令牌 如果不合法 Authorization: breaer xxxx

	// 从Header中获取token
	authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
	tkl := strings.Split(authHeader, " ")
	if len(tkl) != 2 {
		response.Failed(resp, exception.NewUnauthorized("令牌不合法, 格式： Authorization: breaer xxxx"))
		return
	}

	tk := tkl[1]
	logger.L().Debug().Msgf("get token: %s", tk)

	// 检查Token的合法性, 需要通过RPC进行检查
	tkObj, err := a.client.Token().ValidateToken(
		req.Request.Context(),
		token.NewValidateTokenRequest(tk),
	)
	if err != nil {
		response.Failed(resp, exception.NewUnauthorized("令牌校验不合法, %s", err))
		return
	}

	// 放入上下文
	req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)

	// 交给下个处理
	next.ProcessFilter(req, resp)
}
