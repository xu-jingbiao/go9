package chat_test

import (
	"chat"
	"context"
	"os"
	"testing"
)

var (
	ctx = context.Background()
)

func TestSendMessage(t *testing.T) {
	robot := chat.NewChatRobot(os.Getenv("OPEN_API_KEY"))
	req := chat.NewSendMessageRequest("凡人修仙传写的什么\n")
	resp, err := robot.SendMessage(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp.Choices[0].Text)
}

func TestSendMessageCode(t *testing.T) {
	robot := chat.NewChatRobot(os.Getenv("OPEN_API_KEY"))
	req := chat.NewSendMessageRequest(`"""
	Ask the user for their name and say "Hello"
	"""`)
	req.Model = "code-davinci-002"
	resp, err := robot.SendMessage(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(resp.Choices[0].Text)
}

func init() {
}
