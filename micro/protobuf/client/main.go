package main

import (
	"context"
	"fmt"
	"log"

	"gitee.com/go-course/go9/tree/master/micro/protobuf/pb"
	"google.golang.org/grpc"
)

func main() {
	// grpc.Dial负责和gRPC服务建立链接
	conn, err := grpc.Dial("localhost:1234", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// 使用grpc 生成好的代码
	client := pb.NewBlogServiceClient(conn)
	resp, err := client.QueryBlog(context.Background(), &pb.QueryBlogRequest{
		PageSize:   20,
		PageNumber: 1,
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}
