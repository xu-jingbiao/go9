package pb_test

import (
	"testing"

	"gitee.com/go-course/go9/tree/master/micro/protobuf/pb"
	"google.golang.org/protobuf/proto"
)

func TestMarshal(t *testing.T) {
	obj := &pb.String{
		Value: "test",
	}

	data, err := proto.Marshal(obj)
	if err != nil {
		t.Fatal(err)
	}
	// json: {"value": "test"}
	// 二进制: <filed number>:value
	t.Log(data)

	obj_new := &pb.String{}
	err = proto.Unmarshal(data, obj_new)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(obj_new)
}

func TestEnum(t *testing.T) {
	obj := &pb.CreateBlogRequest{
		Status: pb.BLOG_STATUS_PUBLISHED,
	}
	t.Log(obj.Status.String())
}
