#

编译, 在pb目录下执行
```sh
$ cd pb2
# -I PATH, --proto_path=PATH, 指定proto文件搜索的路径, 如果有多个路径 可以多次使用-I 来指定, 如果不指定默认为当前目录
# go 语言的插件: protoc-gen-go : 插件的前缀: protoc-gen 插件的名字:  go
# module 和 protobuf文件里面的go_package 是配套
$ protoc -I=. --go_out=. --go_opt=module="gitee.com/go-course/go9/tree/master/micro/protobuf/pb2" hello.proto
```


```sh
pb/hello.proto: File not found.
hello.proto:6:1: Import "pb/hello.proto" was not found or had errors.
hello.proto:10:5: "Blog" is not defined.
```

编译, 在pb目录下执行
```sh
$ cd pb2
# -I PATH, --proto_path=PATH, 指定proto文件搜索的路径, 如果有多个路径 可以多次使用-I 来指定, 如果不指定默认为当前目录
# go 语言的插件: protoc-gen-go : 插件的前缀: protoc-gen 插件的名字:  go
# module 和 protobuf文件里面的go_package 是配套
$ protoc -I=. -I=.. --go_out=. --go_opt=module="gitee.com/go-course/go9/tree/master/micro/protobuf/pb2" hello.proto

hello.proto:10:5: "Blog" is not defined.
```